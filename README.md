
### Getting Started
You'll need Node.js, `npm` and `gulp`.

Once you've got Node and `npm`, you'll need gulp:

```
$ npm install -g gulp
```

Installs Node Modules

```
$ npm install // install additional dependencies
```

Now you're ready to get going.

## Usage:
---
**IMPORTANT:** Only edit files within the app folder, as all data will be built out into a tmp folder, which should not be edited as it will be over written on each build.
### Local Development Instructions:

```
$ gulp watch
```

This process will watch all your app folders and refresh/build anything new. It will also automatically refresh your browser.

