const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const sassImport 	= require('gulp-sass-import');
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const fontAwesome = require('node-font-awesome');
const image = require('gulp-image');

/* ## Standard Gulp Task
gulp.task('default', () => {
  // place code for your default task here
});
*/

gulp.task('templates', () => {
	const data = require('./locals');
	const options = {
        batch: ['./app/partials', './app/pages'],
    }; 
    return gulp.src(['./app/*.hbs', './app/pages/*.hbs'])
		.pipe(handlebars(data, options))
		.pipe(htmlmin({
			collapseWhitespace: true
		}))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest('./public'));
});
gulp.task('fonts', () => {
    return gulp.src('app/assets/fonts/*')
	    .pipe(gulp.dest('./public/fonts/'));
});

gulp.task('image', () => {
	gulp.src('./app/assets/img/*')
	  .pipe(image())
	  .pipe(gulp.dest('./public/img/'));
  });

gulp.task('sass', () =>
{
    gulp.src('./app/assets/css/style.scss')
        .pipe(sass())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./public/styles'));
});


gulp.task('scripts', () => {
	return gulp.src('./app/assets/js/*.js')
    .pipe(concat('all.min.js'))
		.pipe(gulp.dest('./public/scripts/'));
});

// Static server
gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: "public/"
        }
    });
});

gulp.task('build', ['templates', 'scripts', 'sass', 'fonts', 'image', 'browser-sync'], () => {});

gulp.task('watch', ['build'], () => {
	gulp.watch('./app/*.hbs', ['templates']).on('change', browserSync.reload);
	gulp.watch('./app/pages/*.hbs', ['templates']).on('change', browserSync.reload);
	gulp.watch('./app/partials/*.hbs', ['templates']).on('change', browserSync.reload);
	gulp.watch('./app/assets/css/partials/*.scss', ['sass']).on('change', browserSync.reload);
	gulp.watch('./app/assets/css/*.scss', ['sass']).on('change', browserSync.reload);
	gulp.watch('./app/assets/js/*.js', ['scripts']).on('change', browserSync.reload);
	gulp.watch('./app/assets/img/*', ['image']).on('change', browserSync.reload);
});